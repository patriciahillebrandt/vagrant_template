# Vagrant Templates

## VirtualBox

To use any of the Vagrant Templates available over here, you must have VirtualBox installed in your computer.

If you are using Ubuntu 16.04 as your operating system, you can run the below commands to install the latest VirtualBox:

```
# echo "deb http://download.virtualbox.org/virtualbox/debian xenial contrib" > /etc/apt/sources.list.d/virtualbox.list
# wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | apt-key add -
# apt-get update && apt-get install virtualbox-5.1 -y
```

Otherwise you can download the VirtualBox from their official website: https://www.virtualbox.org/

## Custom Settings

- This Vagrant template file installs all Modern Tribe plugins, tribe-cli and Woocommerce by default;
- It also has a default database with random events ( imported from http://wpshindig.com/events/ );

## Template: wp-xenial

- Ubuntu 16.04
- NGINX (latest available in chris-lea PPA)
- PHP 7.0
- MySQL 5.7
- WordPress (latest version)
- WP-CLI (latest version)
- Composer
- Custom PS1 (including git prompt support)
