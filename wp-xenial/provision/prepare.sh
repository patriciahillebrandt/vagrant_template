#!/bin/bash
#
# Prepare the VM to receive an WordPress instance.
#

basics() {
    # add PPAs
    [[ -f "/etc/apt/sources.list.d/git-core-ubuntu-ppa-xenial.list" ]] || add-apt-repository ppa:git-core/ppa -y
    [[ -f "/etc/apt/sources.list.d/chris-lea-ubuntu-nginx-devel-xenial.list" ]] || add-apt-repository ppa:chris-lea/nginx-devel -y

    # install the basics
    apt-get update
    apt-get install git vim curl unzip -y

    # set timezone
    tz=$(get_config "server_timezone")
    timedatectl set-timezone $tz
}

git_files() {
    # Creates .bash_aliases files
    cp /vagrant/files/home/.bash_aliases /root/
    cp /vagrant/files/home/.bash_aliases /home/ubuntu/

    chown ubuntu:ubuntu /home/ubuntu/.bash_aliases

    # Creates the .gitconfig file
    name=$(get_config "git_name")
    email=$(get_config "git_email")

    cp /vagrant/files/home/.gitconfig /tmp/

    sed -i "s/__name__/$name/g" /tmp/.gitconfig
    sed -i "s/__email__/$email/g" /tmp/.gitconfig

    cp /tmp/.gitconfig /root/
    cp /tmp/.gitconfig /home/ubuntu/

    # Fixes file ownership
    chown ubuntu:ubuntu /home/ubuntu/.gitconfig
}

php_fpm() {
    # Install php-fpm and other extensions
    apt-get install php7.0-fpm php7.0-curl php7.0-cli php7.0-mysql php7.0-xml php7.0-zip php7.0-mbstring php7.0-dev php-pear -y

    # Updates php.ini file
    cp /vagrant/files/php/php.ini /etc/php/7.0/fpm/

    # Creates pool file
    cp /vagrant/files/php/pool.d/ubuntu.conf /etc/php/7.0/fpm/pool.d/

    # Fixes log file ownership
    touch /var/log/php.log
    chown www-data:ubuntu /var/log/php.log

    # Install xdebug
    pecl install xdebug

    # Adds xdebug ini file
    cp /vagrant/files/php/xdebug.ini /etc/php/7.0/mods-available/

    # Enables xdebug module
    phpenmod -s fpm xdebug

    # Restarts php7.0-fpm service
    systemctl restart php7.0-fpm
}

nginx() {
    # Installs nginx
    apt-get install nginx -y

    # Creates document root
    [[ ! -d "/var/www" ]] && mkdir /var/www

    # Updates nginx conf files
    cp /vagrant/files/nginx/nginx.conf /etc/nginx/
    cp /vagrant/files/nginx/conf.d/* /etc/nginx/conf.d/

    # Points sites-available to sites-enabled
    cd /etc/nginx
    rm sites-enabled/default
    cp sites-available/default sites-enabled/
    rm -r sites-available
    ln -sf /etc/nginx/sites-enabled/ sites-available

    # Restarts nginx service
    systemctl restart nginx
}

mysql() {
    mysql_password=$(get_config "mysql_root_passwd")

    debconf-set-selections <<< "mysql-server mysql-server/root_password password $mysql_password"
    debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $mysql_password"

    apt-get install mysql-server-5.7 -y

    cp /vagrant/files/home/.my.cnf /root/

    sed -i "s/xxx/$mysql_password/g" /root/.my.cnf
}

crontab() {
    # Enables cron to cleanup xdebug files
    cp /vagrant/files/cron/local /etc/cron.d/local
}

wpcli() {
    [[ -f "/usr/local/bin/wp" ]] && rm /usr/local/bin/wp

    cd /tmp

    # determines the latest wp-cli release
    version=`curl -s https://api.github.com/repos/wp-cli/wp-cli/tags | egrep -o "v[0-9].[0-9]{1,}.[0-9]" | sort | uniq -d | tail -n1 | sed 's/^v//g'`

    # retrieves wp-cli phar package
    curl -sSL https://github.com/wp-cli/wp-cli/releases/download/v$version/wp-cli-$version.phar -o wp

    if [[ $? -eq 0 ]]; then
        # changes the file permissions
        chmod +x wp

        # moves file to bin directory
        mv wp /usr/local/bin
    fi
}

composer() {
    [[ -f "/usr/local/bin/composer" ]] && rm /usr/local/bin/composer

    cd /tmp

    # retrieves composer phar package
    curl -sS https://getcomposer.org/installer | php

    # moves file to bin directory
    mv composer.phar /usr/local/bin/composer
}

vim() {
    mkdir /root/.vim
    mkdir /home/ubuntu/.vim

    # .vimrc file
    cp /vagrant/files/home/.vimrc /root/
    cp /vagrant/files/home/.vimrc /home/ubuntu/

    # NGINX: syntax highlight
    cd /tmp

    git clone -b master https://github.com/nginx/nginx.git

    cp -R nginx/contrib/vim/* /root/.vim/
    cp -R nginx/contrib/vim/* /home/ubuntu/.vim/

    # fixes files ownership
    chown -R ubuntu:ubuntu /home/ubuntu/.vim*
}

cleanup() {
    [[ -d "/tmp/nginx" ]] && rm -rf /tmp/nginx

    apt-get -f install -y
    apt-get autoclean
}

get_config() {
    grep "$1" /vagrant/files/config | cut -d\" -f2
}

basics
git_files
php_fpm
nginx
mysql
crontab
wpcli
composer
vim

trap cleanup EXIT
