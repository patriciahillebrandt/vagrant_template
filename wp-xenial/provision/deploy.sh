#!/bin/bash

create_db() {
    # Creates the database
    mysql -e "CREATE DATABASE \`$db_name\`;"

    if [[ $? -eq 0 ]]; then
        # Creates the database user
        mysql -e "GRANT ALL ON $db_name.* TO $db_user@localhost IDENTIFIED BY '$db_passwd';"
    else
        echo "Unable to create database."
        exit 1
    fi
}

deploy_wp() {
    target="/var/www/$wp_url"

    mkdir -p $target

    # Downloads latest WordPress
    cd /tmp
    wget -c https://wordpress.org/latest.tar.gz

    # Creates the site root
    tar zxvf latest.tar.gz -C $target --strip=1
    cd $target

    # Generates wp-config.php
    cp wp-config-sample.php wp-config.php

    sed -i "s/database_name_here/$db_name/g" wp-config.php
    sed -i "s/username_here/$db_user/g" wp-config.php
    sed -i "s/password_here/$db_passwd/g" wp-config.php

    # Updates salt keys on wp-config.php
    constants=("AUTH_KEY" "SECURE_AUTH_KEY" "LOGGED_IN_KEY" "NONCE_KEY" "AUTH_SALT" "SECURE_AUTH_SALT" "LOGGED_IN_SALT" "NONCE_SALT")
    salt=$(curl https://api.wordpress.org/secret-key/1.1/salt/)

    for const in "${constants[@]}"; do
        line=$(grep -n "'$const" wp-config.php | cut -d\: -f1)
        sed -i "${line}d" wp-config.php

        key=$(echo "$salt" | grep "'$const")
        sed -i "${line}i$key" wp-config.php
    done

    # Enables WP_DEBUG
    sed -i "s/WP_DEBUG', false/WP_DEBUG', true/" wp-config.php

    # Cleans up unneeded comments
    sed -i "2,21d" wp-config.php

    # Cleans up carriage returns
    sed -i "s/\r$//g" wp-config.php

    # Setup WordPress install
    wp --allow-root core install --url="$wp_url:8080" --title="$wp_title" --admin_user="$wp_user" --admin_password="$wp_passwd" --admin_email="$wp_email" --skip-email

    # Fixes files ownership
    chown -R ubuntu:ubuntu $target
}

mt_plugins_setup() {

    # Removes unnecessary themes
	rm -rf /var/www/$wp_url/wp-content/themes/{twentyfifteen,twentysixteen}

    # Removes unnecessary plugins
	rm -rf /var/www/$wp_url/wp-content/plugins/*

    # Install and activate all Modern Tribe Plugins
    cp /vagrant/files/mt_plugins/* /var/www/$wp_url/wp-content/plugins/
	cd /var/www/$wp_url/wp-content/plugins/
        
    for plugin in `ls /vagrant/files/mt_plugins/ | cut -d/ -f5`; do
        wp plugin install $plugin --allow-root --skip-plugins --skip-themes
    done
	
	# Update and activate all plugins
	wp plugin update --all --allow-root                                                                                                       
    wp plugin activate --all --allow-root --skip-plugins --skip-themes
    rm *.zip

    #Update permissions
    chown -R ubuntu:ubuntu $target
    find $target -type d -exec chmod 755 {} \;
    find $target -type f -exec chmod 644 {} \;

	#import default database
    wp db import /vagrant/files/default.sql --allow-root --skip-plugins --skip-themes
	wp search-replace "http://mt01.com:8080" "http://$wp_url:8080" --allow-root --skip-plugins --skip-themes && wp search-replace "http://www.mt01.com:8080" "http://www.$wp_url:8080" --allow-root --skip-plugins --skip-themes
}

nginx_vhost() {
    cp /vagrant/files/nginx/vhost-template /etc/nginx/sites-enabled/$wp_url

    sed -i "s/__domain__/$wp_url/g" /etc/nginx/sites-enabled/$wp_url

    nginx -t && systemctl restart nginx
}

get_config() {
    grep "$1" /vagrant/files/config | cut -d\" -f2
}

cleanup() {
    [[ -f "/tmp/latest.tar.gz" ]] && rm /tmp/latest.tar.gz
}

db_name=$(get_config "site_db_name")
db_user=$(get_config "site_db_user")
db_passwd=$(get_config "site_db_passwd")
create_db

wp_url=$(get_config "site_wp_url" | sed "s/^www\.//g")
wp_title=$(get_config "site_wp_title")
wp_email=$(get_config "site_wp_email")
wp_user=$(get_config "site_wp_user")
wp_passwd=$(get_config "site_wp_passwd")
deploy_wp

mt_plugins_setup

nginx_vhost

trap cleanup EXIT
