# Deny WPScan
if ($http_user_agent ~* "WPScan") { return 444; }

# Deny access to /vendor, /.git and /README.md
if ($request_uri ~ "/(\.git|README\.md)") { return 444; }

# WordPress Hardening
if ($request_uri ~ ^/wp-admin/includes/) { return 403; }
if ($request_uri ~ ^/wp-includes/js/tinymce/langs/.+\.php$) { return 403; }
if ($request_uri ~ ^/wp-includes/theme-compat/) { return 403; }
if ($request_uri ~ ^/wp-config\.php$) { return 403; }
if ($request_uri ~ ^/wp-includes/[^/]+\.php$) { return 403; }
