# Terminal Title
export PROMPT_COMMAND="echo -ne \"\033]0;$1$USER@$HOSTNAME\007\""

# Set default editor
export EDITOR=vim

# Git PS1
source /usr/lib/git-core/git-sh-prompt
export PS1='\[\e[90m\][\[\e[34m\]\[\e[4m\]\h\[\e[m\] \[\e[90m\]\w\[\e[m\]$(__git_ps1 " \[\e[33m\](%s)\[\e[m\]")\[\e[90m\]]\$\[\e[m\] '
