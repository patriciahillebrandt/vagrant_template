set t_Co=256

set paste
set encoding=utf-8
set fileencoding=utf-8
set backspace=indent,eol,start
set cursorline
set history=1000
set expandtab
set shiftwidth=4
set softtabstop=4
set tabstop=4
set autoindent
set title
set ffs=unix,mac,dos

set ruler
set rulerformat=%l,%v

set viminfo='10,\"100,:20,%,n~/.viminfo

hi CursorLine cterm=NONE ctermbg=234 guibg=;1c1c1c

autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

filetype plugin indent on

autocmd Filetype html,css,javascript,yaml,xml setlocal ts=2 sts=2 sw=2 et
